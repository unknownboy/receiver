package com.example.receiver;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
     static int timeHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
     static int timeMinute = Calendar.getInstance().get(Calendar.MINUTE);
    static TextView textView1;
     static TextView textView2;
    public static TextView getTextView2() {
        return textView2;
    }


    static  AlarmManager alarmManager[];
    SharedPreferences sp;
    static PendingIntent pendingIntent[];
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    int alarms;
    static TextView tt;
    static  Button cancel;

    static  Button from;
    static  Button to;
    static TextView fromfield;
    static TextView tofield;
    static Button msgBtn;
    static AutoCompleteTextView msgEdit;
    static TextView msgView;
    static Button btn1;
    static Button btn2;
    static Button infoActivity;
    static Button setTemplate;

    public static TextView fromf() {
        return fromfield;
    }
    public static TextView tof() {
        return tofield;
    }



    static final String FILE="Receiver_file";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mylayout);

       sp=getSharedPreferences(FILE,Context.MODE_PRIVATE);
       final SharedPreferences.Editor ed= sp.edit();


        textView1 = (TextView)findViewById(R.id.msg10);
        textView1.setText(timeHour + ":" + timeMinute);
        textView2 = (TextView)findViewById(R.id.msg2);

        from=findViewById(R.id.from);
        to=findViewById(R.id.to);
       // cancel=findViewById(R.id.cancel);
        fromfield=findViewById(R.id.msg1);
        tofield=findViewById(R.id.msg2);
        msgBtn=findViewById(R.id.set);
        msgEdit=findViewById(R.id.msgEdit);
        msgView=findViewById(R.id.msgView);
        infoActivity=findViewById(R.id.btn1);
        setTemplate=findViewById(R.id.setTemplate);
        infoActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Main2Activity.class));
            }
        });


        if(!sp.getBoolean("signup",false)){
            startActivity(new Intent(this,Main3Activity.class));
        }

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.READ_PHONE_STATE,
                android.Manifest.permission.SEND_SMS,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE
        };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        String sstt=sp.getString("mymsg","Busy...Call You Later");

    msgView.setText(sstt);
    AlarmReceiver.mymsg=sstt;

    ArrayAdapter<String> arrayadapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,sp.getString("templates","Busy...Call You Later").split("X-X"));
    System.out.println(sp.getString("templates","Busy...Call You Later").split("ioi").length);
    msgEdit.setAdapter(arrayadapter);


    msgBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String str=msgEdit.getText().toString();
            msgView.setText(str);
            AlarmReceiver.mymsg=str;
            msgEdit.setText("");
            ed.putString("mymsg",str);
            ed.commit();
        }
    });

    setTemplate.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ed.putString("templates",sp.getString("templates","Busy...Call You Later")+"ioi"+msgEdit.getText());
            ed.commit();
            Toast.makeText(getApplicationContext(), msgEdit.getText().toString()+" Added To Templates", Toast.LENGTH_SHORT).show();
            Set <String> s = new HashSet<String>(Arrays.asList(sp.getString("templates","Busy...Call You Later").split("ioi")));
            String[] arr =Arrays.copyOf(s.toArray(), s.size(), String[].class);

            ArrayAdapter<String> arrayadapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,arr);
            System.out.println(sp.getString("templates","Busy...Call You Later").split("ioi").length);
            msgEdit.setAdapter(arrayadapter);
        }
    });

        alarmManager=new AlarmManager[2];
        pendingIntent=new PendingIntent[2];
        alarmManager[0] = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent myIntent[] = {new Intent(MainActivity.this, com.example.receiver.AlarmReceiver.class),new Intent(MainActivity.this, com.example.receiver.AlarmReceiver2.class)};
        pendingIntent[0]= PendingIntent.getBroadcast(MainActivity.this, 0, myIntent[0], 0);
        alarmManager[1] = (AlarmManager) getSystemService(ALARM_SERVICE);
        pendingIntent[1]= PendingIntent.getBroadcast(MainActivity.this, 0, myIntent[1], 0);

        View.OnClickListener listener1 = new View.OnClickListener() {
            public void onClick(View view) {
                textView2.setText("");
                Bundle bundle = new Bundle();
                bundle.putInt(com.example.receiver.MyConstants.HOUR, timeHour);
                bundle.putInt(com.example.receiver.MyConstants.MINUTE, timeMinute);
                MyDialogFragment fragment;
                fragment = new MyDialogFragment(new MyHandler());
                fragment.setArguments(bundle);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(fragment, com.example.receiver.MyConstants.TIME_PICKER);
                transaction.commit();
            }
        };

        btn1 = (Button)findViewById(R.id.from);
        btn1.setOnClickListener(listener1);


//        View.OnClickListener listener2 = new View.OnClickListener() {
//            public void onClick(View view) {
//                textView2.setText("");
//                cancelAlarm(getApplicationContext());
//            }
//        };
//        btn2 = (Button)findViewById(R.id.cancel);
//        btn2.setOnClickListener(listener2);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(MainActivity.this, "Feature Not Supported Yet!!!", Toast.LENGTH_SHORT).show();
//            }
//        });

        View.OnClickListener listener3 = new View.OnClickListener() {
            public void onClick(View view) {
                textView2.setText("");
                Bundle bundle = new Bundle();
                bundle.putInt(com.example.receiver.MyConstants.HOUR, timeHour);
                bundle.putInt(com.example.receiver.MyConstants.MINUTE, timeMinute);
                MyDialogFragment fragment;
                fragment = new MyDialogFragment(new MyHandler2());
                fragment.setArguments(bundle);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(fragment, com.example.receiver.MyConstants.TIME_PICKER);
                transaction.commit();
            }
        };

        to.setOnClickListener(listener3);


    }
    static class MyHandler extends Handler {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void handleMessage (Message msg){
            Bundle bundle = msg.getData();
            timeHour = bundle.getInt(com.example.receiver.MyConstants.HOUR);
            timeMinute = bundle.getInt(com.example.receiver.MyConstants.MINUTE);
            fromfield.setText(timeHour + ":" + timeMinute);
            setAlarm1();
        }
    }
    static class MyHandler2 extends Handler {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void handleMessage (Message msg){
            Bundle bundle = msg.getData();
            timeHour = bundle.getInt(com.example.receiver.MyConstants.HOUR);
            timeMinute = bundle.getInt(com.example.receiver.MyConstants.MINUTE);
            tofield.setText(timeHour + ":" + timeMinute);
            setAlarm2();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
     static void setAlarm1(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, timeHour);
        calendar.set(Calendar.MINUTE, timeMinute);
        alarmManager[0].setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent[0]);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
     static void setAlarm2(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, timeHour);
        calendar.set(Calendar.MINUTE, timeMinute);
        alarmManager[1].setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent[1]);
    }

//      void cancelAlarm(Context c) {
//        if (alarmManager!= null) {
//            AlarmReceiver.alarm=false;
//            SharedPreferences sp= getSharedPreferences(FILE,Context.MODE_PRIVATE);
//            SharedPreferences.Editor ed=sp.edit();
//            ed.putBoolean("alarm",false);
//            ed.commit();
//
//            alarmManager[0].cancel(pendingIntent[0]);
//            alarmManager[1].cancel(pendingIntent[1]);
//            fromfield.setText("");
//            tofield.setText("");
//            Toast.makeText(c,"Alarm Disabled",Toast.LENGTH_LONG).show();
//            System.out.println(AlarmReceiver.alarm);
//        }
//    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
