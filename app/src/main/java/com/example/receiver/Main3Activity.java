package com.example.receiver;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    static EditText name;
    static EditText phone;
    static Button register;
    static TextView tv;

    static SharedPreferences sp;
    static SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        name=findViewById(R.id.nameEdit);
        phone=findViewById(R.id.editText2);
        register=findViewById(R.id.button3);
        tv=findViewById(R.id.textView2);
        sp=getSharedPreferences(MainActivity.FILE, Context.MODE_PRIVATE);
        ed=sp.edit();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String myname = name.getText().toString();
                String myphone = phone.getText().toString();
                //method.setText("Get Method");
                new SigninActivity(getApplicationContext(),tv,tv,1).execute(myname,myphone);
                ed.putBoolean("signup",true);
                ed.commit();
                tv.setText("Thank You...!!!");
                try{
                    Thread.sleep(5000);
                }catch (Exception e){}

                finish();
            }

        });

    }
}
